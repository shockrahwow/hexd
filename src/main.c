#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // used for access() safety check 

#include "header.h"
#include "hex.h"

#define BSIZE 8 /* size of our peeking buffer */
#define SSIZE 4 /* spacing between groups */

// debug flags
#define ADEBUG 1

/* Hex dump of whole file */
static void fhexdump(const char* fileName);

void HeadHandler(const char* fileName);

int main(int argc, char** argv) {
	// setting up our arguments
	int headFlag = 0;
	int hexFlag = 0;
	char* fileTarget = NULL;
	int c;
	if(argc == 1) {
		// ebin help thing
		printf("Usage:./xd -f targetFile -m(header info) -d(hexdump)\n");
		return 0;
	}
	/* checking the arguments*/
	while((c=getopt(argc, argv, "mdhf:")) != -1) {
		switch(c) {
			case 'm':
				headFlag = 1;
				break;
			/* hexdumping flag */
			case 'd':
				hexFlag = 1;
				break;
			/* requires a file to be passed as an arg to this flag */
			case 'f': {
				fileTarget = optarg;
				if(!fileTarget) {
					return 1;
				}
				break;
			}
			case 'h':
				printf("Usage:./xd -f targetFile -m(header info) -d(hexdump)\n");
				return 0;
		}
	}

	if(!fileTarget) {
		printf("No target provided\n");
		return 1;
	}

	if(headFlag)
		HeadHandler(fileTarget);

	if(hexFlag)
		fhexdump(fileTarget);

	return 0;
}

/* Hex dump of whole file */
static void fhexdump(const char* fileName) {
	FILE* file = fopen(fileName, "rb");
	basic_hex(file, 0);
	fclose(file);
}

/* 
 * Here we dump out pretty much evertyhing about the header that we can
 * */
void HeadHandler(const char* fileName) {
	FILE* file = fopen(fileName, "rb");
	headerInfoDispatch(file);
	fclose(file);
}
