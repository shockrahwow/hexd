#include <assert.h>
#include "header.h"

HeaderData64_t* pullHeaderData(FILE* file) {
	HeaderData64_t* retData = (malloc)(sizeof(HeaderData64_t));
	
	// one last error checking thingy after reading in the data to our return able buffer
	size_t bytesRead = fread(retData, 0x01, sizeof(HeaderData64_t), file);

	if(bytesRead != MaxHeaderLength)
		fprintf(stderr, "Warning only read [%ld] into buffer\n", bytesRead);
	
	return retData;
}

SectionHeader64_t* pullSectionHeader(HeaderData64_t* fileHeader, FILE* file) {
	uint64_t offset = fileHeader->sectionHeaderTable;
	// if fseek fails then can't reliably do anything from here out
	int _seek = fseek(file, offset, SEEK_SET);
	if(_seek != 0) {
		return NULL;
	}

	// read N bytes from the offset
	SectionHeader64_t* data = malloc(sizeof(SectionHeader64_t));

	size_t bytes = fread(data, 0x01, sizeof(SectionHeader64_t), file);
	if(bytes != sizeof(SectionHeader64_t)) {
		free(data);
		return NULL;
	}

	return data;
}

/*
 * Below are functions which prinout tab delimited header data
 * */
void showHeader(const HeaderData64_t* head) {
	printf("Architecture \t%u\n", head->architecture);
	printf("Encoding \t%u\n", head->encoding);
	printf("OSABI \t%u\n", head->osABI);
	// skipping padding
	printf("ELFType\t%u\n", (unsigned short)head->elfType);
	printf("ISA \t%u\n", (unsigned short)head->instructionSet);
	printf("ELFVersion\t%lu\n", (unsigned long)head->elfVersion);

	printf("ProgramEntry\t0x%lx\n", head->progEntry);
	printf("ProgramTablePosition\t0x%lx\n" , head->progTablePosition);
	printf("SectionTablePosition\t0x%lx\n", head->sectionHeaderTable);

	printf("Flags\t0x%lx\n", (long unsigned)head->flags);
	printf("HeaderSize\t0x%x\n", (unsigned short)head->headerSize);

	printf("ProgramHeaderEntrySize\t0x%x\n", head->progHeaderEntrySize);
	printf("ProgramHeaderEntryCount\t0x%lx\n", (unsigned long)head->progHeaderEntyCount);
	
	printf("SectionHeaderEntrySize\t0x%x\n", head->sectHeaderEntrySize);
	printf("SectionHeaderEntryCount\t0x%x\n", head->sectHeaderEntryCout);

	printf("SectionNamesIndex\t0x%x\n", head->sectNamesIdx);
}

void showSectionHeader(const SectionHeader64_t* head) {
	assert(head != NULL);

	printf("\n");
	printf("SectionHeaderName\t%u\n", head->sh_name);
	printf("SectionHeaderType\t%u\n", head->sh_type);
	printf("SectionHeaderFlags\t0x%16lx\n", head->sh_flags);

	printf("SectionHeaderAddress\t0x%16lx\n", head->sh_addr);
	printf("SectionHeaderOffset\t0x%16lx\n", head->sh_offset);

	printf("SectionHeaderSize\t%lu\n", head->sh_size);
	printf("SectionHeaderLink\t%lu\n", head->sh_link);
	printf("SectionHeaderInfo\t%lu\n", head->sh_info);

	printf("SectionHeaderAddrAlign\t0x%16lx\n", head->sh_addralign);
	printf("SectionHeaderAddrAlign\t%lu\n", head->sh_entsize);
}

void __showHexSlice(const uint8_t* buffer, const int size) {
	for(int i = 0;i < size;i++) {
		printf("%2x", buffer[i]);
	}
	printf("\n");
}

void headerInfoDispatch(FILE* file) {
	// Main header data
	HeaderData64_t* f_header = pullHeaderData(file); //  malloc
	if(f_header) {
		showHeader(f_header);
	}

	// Program Header


	// Section Header
	SectionHeader64_t* s_header = pullSectionHeader(f_header, file);
	if(s_header) {
		showSectionHeader(s_header);
		free(s_header);
	}
}



